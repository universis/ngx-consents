/*
 * Public API Surface of ngx-consents
 */

export * from './lib/ngx-consent.service';
export * from './lib/ngx-consent.component';
export * from './lib/ngx-consent.module';
