import { Injectable, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { from, map, shareReplay } from 'rxjs';
import { NgxConsentComponent } from './ngx-consent.component';

function mapToBoolean(value: any) {
  Object.keys(value).forEach((key) => {
    const group = value[key];
    Object.keys(group).forEach((key) => {
      const field = group[key];
      if (Object.prototype.hasOwnProperty.call(field, 'val')) {
        field.bool = (field.val === 'y') || (field.val === 'dy');
      }
    });
  });
  return value;
}

function mapFromBoolean(value: any) {
  Object.keys(value).forEach((key) => {
    const group = value[key];
    Object.keys(group).forEach((key) => {
      const field = group[key];
      if (Object.prototype.hasOwnProperty.call(field, 'bool')) {
        field.val = field.bool ? 'y' : 'n';
        delete field.bool;
      }
    });
  });
  return value;
}

export class NgxConsentGroupService {
  constructor(private name: string, private context: AngularDataContext) { }

  public values: { [k: string]: any } = from(this.context.model(`users/me/consents/${this.name}`).asQueryable().getItem()).pipe(
    map(({ value }) => {
      return mapToBoolean(value);
    }),
  );
}

@Injectable()
export class NgxConsentService {
  modalRef: any;

  constructor(private context: AngularDataContext, private modal: BsModalService) { }

  public values = from(this.context.model('users/me/consents').asQueryable().getItem()).pipe(
    map(({ value }) => {
      return mapToBoolean(value);
    }),
    shareReplay()
    );

  public group(name: string) {
    return new NgxConsentGroupService(name, this.context);
  }

  set(values: { [k: string]: any }) {
    const data = mapFromBoolean(values);
    return this.context.model('users/me/consents').save(data);
  }


}
