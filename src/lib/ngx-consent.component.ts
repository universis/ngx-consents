import { AfterViewInit, Component, Input, EventEmitter, Output } from '@angular/core';
import { NgxConsentService } from './ngx-consent.service';
import { map } from 'rxjs';
import { LoadingService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'universis-consents',
  template: `
    <div>
      <formio [form]="form" [renderOptions]="renderOptions"
        [submission]="{ data }" (customEvent)="onEvent($event)"></formio>
    </div>
    <ng-container *ngIf="lastError">
      <div class="mt-2">
        <div class="alert alert-danger" role="alert">
          {{ lastError.message }}
        </div>
      </div>
    </ng-container>
  `,
  styles: [
  ]
})
export class NgxConsentComponent implements AfterViewInit {

  @Output() submitted: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<any> = new EventEmitter();
  data: { [k: string]: any; };
  lastError: any;

  onEvent(event: any) {
    if (event.type === 'save') {
      this.lastError = null;
      this.loading.showLoading();
      this.service.set(event.data).then(() => {
        this.loading.hideLoading();
        this.submitted.emit();
      }).catch((err) => {
        this.loading.hideLoading();
        this.lastError = err.error ? err.error : err;
      });
    }
  }
  form: { components: any[]; };
  renderOptions: { language: string; i18n: any; }
  @Input() group: string = null;

  constructor(
    public service: NgxConsentService,
    private loading: LoadingService,
    private translate: TranslateService) { }

  ngAfterViewInit(): void {
    const source = this.group ? this.service.group(this.group).values : this.service.values;
    source.pipe(map((groups) => {
      return this.getForm(groups);
    })).subscribe((form: { components: any[] }) => {
      this.form = form;
      this.renderOptions = {
        language: this.translate.currentLang,
        i18n: {
          [this.translate.currentLang]: {
            "Submit": this.translate.instant('Forms.Submit'),
          }
        }
      }
    });
  }

  getForm(values: { [k: string]: any }) {
    this.data = values;
    const components: any[] = Object.keys(values).filter((group) => {
      const keys = Object.keys(values[group]);
      return keys.length > 1;
    }).map((groupKey) => {
      const group = values[groupKey];
      return {
        "key": "fieldSet",
        "type": "fieldset",
        "customClass": "consent-group font-weight-normal",
        "legend": group.metadata?.name,
        "input": false,
        "tableView": false,
        "components": Object.keys(group).filter((key) => {
          return key !== 'metadata'
        }).map((key) => {
          const field = group[key];
          return {
            "label": field.metadata.name,
            "description": field.metadata.description,
            "tableView": false,
            "disabled": !!field.metadata.archived,
            "key": `${groupKey}.${key}.bool`,
            "type": "checkbox",
            "input": true
          };
        })
      };
    });
    if (components.length > 0) {
      components.push(
        {
          "label": "Submit",
          "action": "event",
          "showValidations": false,
          "disableOnInvalid": true,
          "key": "submit",
          "theme": "theme",
          "type": "button",
          "input": false,
          "event": "save"
        }
      );
    }
    return {
      components
    };
  }

}
