import { TestBed } from '@angular/core/testing';

import { NgxConsentService } from './ngx-consent.service';

describe('NgxConsentsService', () => {
  let service: NgxConsentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxConsentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
