# @universis/ngx-consents

[Universis Consents](https://gitlab.com/universis/consents) Components for Angular

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

## Installation

```bash
npm install @universis/ngx-consents
```

## Usage

```typescript
import { NgxConsentsModule } from '@universis/ngx-consents';
```

